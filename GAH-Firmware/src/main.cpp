#include <Arduino.h>
#include "../lib/serial-command-handler/CommandManager.h"


//PID Variables
double p_id = 1.0;
double i_id = 1.0;
double d_id = 1.0;
double desiredTemp = 35;
double unifiedTemp = 0;
uint16_t tempA = 0;
uint16_t tempB = 0;
uint8_t heatPWM = 0;

//Pin Definitions
#define TempAPin A0
#define TempBPin A1
#define heaterPin 3

//Heater State
bool heaterOn = false;
bool pidMode = false;

/*
  * Temperature Lookup Table
  -Defined for a 10K pull down reference resistor
  -Defined for a 10K NTC Thermistor with a Beta = 3630K (SMD 1206)
  -Defined for a 3.3VDC supply
*/
#define lookUpTableSize 4
const int temperatures[lookUpTableSize] = {40,30,20,10}; 
const uint16_t rawTemperatureValues[lookUpTableSize]  = {658, 565, 460, 354};


/* getTemperature(rawADCValue) NEEDS TO BE UPDATED AND VERIFIED
  -Returns the temperature in degrees Celsius.
  -Accepts a raw ADC reading value (i.e. 0-1023).
  -Uses the temperature lookup table declared in config file.
*/
float getTemperature(uint16_t rawValue) {
  int lowerBound = 0;
  if(rawValue < rawTemperatureValues[0]) {
    return temperatures[0];
  }
  else if(rawValue < rawTemperatureValues[lookUpTableSize]) {
    return temperatures[lookUpTableSize];
  }
  for(int i = 0; i < lookUpTableSize-1; i++) {
    if(rawValue > rawTemperatureValues[i] && rawValue < rawTemperatureValues[i+1]) {
      lowerBound = i;
    }
  }
  return map(rawValue,rawTemperatureValues[lowerBound],rawTemperatureValues[lowerBound+1],temperatures[lowerBound],temperatures[lowerBound+1]);
}

/* updatePWM()
  -Does not return anything
  -Accesses only global variable
  -updated the PWM on the heater line only if the heater is switched on.
*/
void updatePWM() {
  if(heaterOn) {
    float tempDif = desiredTemp - unifiedTemp;
    if(pidMode) {

    }
    else {
      digitalWrite(heaterPin, map(tempDif, 0,30,0,255));
    }
  }
  else {
    digitalWrite(heaterPin,LOW);
  }
}

/*
  * Temperature Lookup Table
  -Defined for a 20K pull down reference resistor
  -Defined for a 10K NTC Thermistor with a Beta = 3435K
  -Defined for a 12VDC supply
*/
//#define lookUpTableSize 21
//const int temperatures[lookUpTableSize] = {40,35,30,25,20,15,10,5,0,-1,-2,-3,-4,-5,-6,-7,-8,-9,-10,-20,-30};
//const int rawTemperatureValues[lookUpTableSize]  = {954,914,869,819,763,703,640,572,504,490,477,463,450,436,422,409,396,384,370,251,157};

//Communication Manager
CommandManager comms;

void setup() {
  pinMode(heaterPin,OUTPUT); //Heater Pin
  pinMode(TempAPin,INPUT); // Therm Pin A
  pinMode(TempBPin,INPUT); // Therm Pin B
  Serial.begin(9600);
  //Build Command Manager
  comms.addCommand(0,'t',0,[] (char) {
    Serial.println();
    Serial.print("Temperature: ");
    Serial.print(unifiedTemp);
    Serial.println();
  });
  comms.addCommand(1,'o',1,[] (char _buff) {
    if(_buff == '1') {
      heaterOn = true;
      Serial.println();
      Serial.println("Heater ON");
    }
    else {
      heaterOn = false;
      Serial.println();
      Serial.println("Heater OFF");
    }
  });
}

void loop() {
  // Check UART Port
  comms.check();
  // Update Temperature PWM
  tempA = analogRead(TempAPin);
  tempB = analogRead(TempBPin);
  unifiedTemp = getTemperature((tempA+tempB)/2);
}
